package io.github.ashy1227.unbadredstone;

import com.terraformersmc.modmenu.api.ConfigScreenFactory;
import com.terraformersmc.modmenu.api.ModMenuApi;
import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.ConfigHolder;
import me.shedaniel.clothconfig2.api.ConfigBuilder;
import me.shedaniel.clothconfig2.api.ConfigCategory;
import me.shedaniel.clothconfig2.api.ConfigEntryBuilder;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;

import java.util.Optional;

public class UnbadRedstoneModMenuApiImpl implements ModMenuApi {
	@Override
	public ConfigScreenFactory<?> getModConfigScreenFactory() {
		return parent -> {
			ConfigHolder<UnbadRedstoneConfig> configHolder = AutoConfig.getConfigHolder(UnbadRedstoneConfig.class);
			UnbadRedstoneConfig config = configHolder.get();
			
			ConfigBuilder builder = ConfigBuilder.create()
				.setParentScreen(parent)
				.setTitle(Text.translatable("title.unbad-redstone.config"))
				.setSavingRunnable(configHolder::save);
			
			ConfigEntryBuilder entryBuilder = builder.entryBuilder();
			
			ConfigCategory general = builder.getOrCreateCategory(Text.translatable("category.unbad-redstone.general"));
			general.addEntry(entryBuilder.startEnumSelector(Text.translatable("option.unbad-redstone.mode"), UnbadRedstoneConfig.Mode.class, config.getMode())
				.setEnumNameProvider(mode -> Text.translatable("option.unbad-redstone.mode." + mode.name().toLowerCase()))
				.setTooltipSupplier(mode -> Optional.of(new MutableText[] {Text.translatable("option.unbad-redstone.mode." + mode.name().toLowerCase() + ".tooltip")}))
				.setDefaultValue(UnbadRedstoneConfig.Mode.OLD)
				.setSaveConsumer(newValue -> config.mode = newValue.name())
				.build()
			);
			
			return builder.build();
		};
	}
}
