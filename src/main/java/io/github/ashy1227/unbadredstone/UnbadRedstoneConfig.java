package io.github.ashy1227.unbadredstone;

import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.annotation.Config;

@Config(name = "unbad-redstone")
public class UnbadRedstoneConfig implements ConfigData {
	public String mode = Mode.OLD.name();
	
	public Mode getMode() {
		switch(this.mode) {
			case "DISABLED":
				return Mode.DISABLED;
			case "ENABLED":
				return Mode.ENABLED;
			case "OLD":
				return Mode.OLD;
		}
		return Mode.DISABLED;
	}
	
	public enum Mode {
		DISABLED, ENABLED, OLD
	}
}