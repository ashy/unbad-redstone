package io.github.ashy1227.unbadredstone.mixin;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import io.github.ashy1227.unbadredstone.UnbadRedstoneConfig;
import me.shedaniel.autoconfig.AutoConfig;
import net.minecraft.block.*;
import net.minecraft.block.enums.WireConnection;
import net.minecraft.state.property.EnumProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Map;

@Mixin(RedstoneWireBlock.class)
public class RedstoneWireBlockMixin extends Block {
	@Shadow @Final public static Map<Direction, EnumProperty<WireConnection>> DIRECTION_TO_WIRE_CONNECTION_PROPERTY;
	
	private static final VoxelShape CUSTOM_DOT_SHAPE;
	private static final Map<Direction, VoxelShape> CUSTOM_DIRECTION_TO_SIDE_SHAPE;
	private static final Map<Direction, VoxelShape> CUSTOM_DIRECTION_TO_UP_SHAPE;
	private static final Map<BlockState, VoxelShape> CUSTOM_SHAPES = Maps.newHashMap();
	
	private UnbadRedstoneConfig config;
	
	public RedstoneWireBlockMixin(Settings settings) {
		super(settings);
	}
	
	@Inject(at = @At("TAIL"), method = "Lnet/minecraft/block/RedstoneWireBlock;<init>(Lnet/minecraft/block/AbstractBlock$Settings;)V")
	private void init(AbstractBlock.Settings settings, CallbackInfo ci) {
		this.config = AutoConfig.getConfigHolder(UnbadRedstoneConfig.class).getConfig();
		
		for(BlockState blockState : this.getStateManager().getStates()) {
			if(blockState.get(Properties.POWER) != 0) {
				continue;
			}
			CUSTOM_SHAPES.put(blockState, this.customGetShapeForState(blockState));
		}
	}
	
	@Inject(at = @At("HEAD"), cancellable = true, method = "Lnet/minecraft/block/RedstoneWireBlock;getOutlineShape(Lnet/minecraft/block/BlockState;Lnet/minecraft/world/BlockView;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/ShapeContext;)Lnet/minecraft/util/shape/VoxelShape;")
	private void getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context, CallbackInfoReturnable<VoxelShape> cir) {
		if(config.getMode() == UnbadRedstoneConfig.Mode.ENABLED) {
			cir.setReturnValue(CUSTOM_SHAPES.get(state.with(Properties.POWER, 0)));
		} else if(config.getMode() == UnbadRedstoneConfig.Mode.OLD) {
			cir.setReturnValue(CUSTOM_DOT_SHAPE);
		}
	}
	
	private VoxelShape customGetShapeForState(BlockState state) {
		VoxelShape voxelShape = CUSTOM_DOT_SHAPE;
		for(Direction direction : Direction.Type.HORIZONTAL) {
			WireConnection wireConnection = state.get(DIRECTION_TO_WIRE_CONNECTION_PROPERTY.get(direction));
			if(wireConnection == WireConnection.SIDE) {
				voxelShape = VoxelShapes.union(voxelShape, CUSTOM_DIRECTION_TO_SIDE_SHAPE.get(direction));
				continue;
			}
			if(wireConnection != WireConnection.UP) {
				continue;
			}
			voxelShape = VoxelShapes.union(voxelShape, CUSTOM_DIRECTION_TO_UP_SHAPE.get(direction));
		}
		return voxelShape;
	}
	
	static {
		CUSTOM_DOT_SHAPE = Block.createCuboidShape(0.0d, 0.0d, 0.0d, 16.0d, 1.0d, 16.0d);
		
		CUSTOM_DIRECTION_TO_SIDE_SHAPE = Maps.newEnumMap(
			ImmutableMap.of(
				Direction.NORTH, Block.createCuboidShape(0.0d, 0.0d, 0.0d, 16.0d, 1.0d, 16.0d),
				Direction.SOUTH, Block.createCuboidShape(0.0d, 0.0d, 0.0d, 16.0d, 1.0d, 16.0d),
				Direction.EAST, Block.createCuboidShape(0.0d, 0.0d, 0.0d, 16.0d, 1.0d, 16.0d),
				Direction.WEST, Block.createCuboidShape(0.0d, 0.0d, 0.0d, 16.0d, 1.0d, 16.0d)
			)
		);
		
		CUSTOM_DIRECTION_TO_UP_SHAPE = Maps.newEnumMap(
			ImmutableMap.of(
				Direction.NORTH, VoxelShapes.union(CUSTOM_DIRECTION_TO_SIDE_SHAPE.get(Direction.NORTH),
					Block.createCuboidShape(0.0d, 0.0d, 0.0d, 16.0d, 16.0d, 1.0d)
				),
				Direction.SOUTH, VoxelShapes.union(CUSTOM_DIRECTION_TO_SIDE_SHAPE.get(Direction.SOUTH),
					Block.createCuboidShape(0.0d, 0.0d, 15.0d, 16.0d, 16.0d, 16.0d)
				),
				Direction.EAST, VoxelShapes.union(CUSTOM_DIRECTION_TO_SIDE_SHAPE.get(Direction.EAST),
					Block.createCuboidShape(15.0d, 0.0d, 0.0d, 16.0d, 16.0d, 16.0d)
				),
				Direction.WEST, VoxelShapes.union(CUSTOM_DIRECTION_TO_SIDE_SHAPE.get(Direction.WEST),
					Block.createCuboidShape(0.0d, 0.0d, 0.0d, 1.0d, 16.0d, 16.0d)
				)
			)
		);
	}
}
