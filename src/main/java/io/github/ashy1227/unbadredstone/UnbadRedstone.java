package io.github.ashy1227.unbadredstone;

import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;
import net.fabricmc.loader.api.entrypoint.PreLaunchEntrypoint;

public class UnbadRedstone implements PreLaunchEntrypoint {
	@Override
	public void onPreLaunch() {
		AutoConfig.register(UnbadRedstoneConfig.class, GsonConfigSerializer::new);
	}
}
