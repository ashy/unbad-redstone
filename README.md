# Unbad Redstone
Makes redstone hitboxes full squares instead of annoying tiny hitboxes that are easy to miss

## License

[The Unlicense](LICENSE)
